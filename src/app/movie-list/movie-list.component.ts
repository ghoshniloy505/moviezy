import { Component } from '@angular/core';
import { MovieService } from '../movie.service';
import { Router } from '@angular/router';
import { Movie } from '../movie';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-movie-list',
  templateUrl: './movie-list.component.html',
  styleUrls: ['./movie-list.component.scss'],
})
export class MovieListComponent {
  movies: Movie[] = [];
  filteredMovies: Movie[] = [];

  constructor(
    private _movieService: MovieService,
    private _router: Router,
    private _toastr: ToastrService
  ) {}

  ngOnInit(): void {
    this.getMovies();
  }

  private getMovies() {
    let timer: number;
    let tookLong = false;

    timer = window.setTimeout(() => {
      tookLong = true;
      this._toastr.warning('App spun down due to inactivity. Shall be initialized soon!.', undefined, { timeOut: 0 });
    }, 2000);

    this._movieService.getMoviesList().subscribe((data) => {
      clearTimeout(timer);
      this.movies = data;
      if (tookLong) {
        this._toastr.clear();
        this._toastr.success('We\'re up! Thanks for your patience!');
      }
    }, (error) => {
      clearTimeout(timer);
      this._toastr.clear();
      alert(error);
    });
  }

  updateMovie(id: number) {
    this._router.navigate(['update-movie', id]);
  }

  deleteMovie(id: number) {
    if (
      confirm('Are you sure you want to remove this movie from the watchlist?')
    ) {
      this._movieService.deleteMovie(id).subscribe({
        next: () => {
          this.getMovies();
          this._toastr.success('Movie Removed from Watchlist');
        },
        error: (error) => {
          alert(error);
        },
      });
    }
  }

  movieDetails(id: number) {
    this._router.navigate(['movie-details', id]);
  }

  setBadgeColor(value: number): string {
    if (value >= 0 && value < 5) {
      return 'badge rounded-pill text-bg-danger';
    } else if (value >= 5 && value < 8) {
      return 'badge rounded-pill text-bg-warning';
    } else {
      return 'badge rounded-pill text-bg-success';
    }
  }
}
